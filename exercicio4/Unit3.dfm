object DataModule3: TDataModule3
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 454
  Width = 191
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    VendorLib = 'C:\EasyPHP-DevServer-14.1VC9\binaries\mysql\lib\libmysql.dll'
    Left = 56
    Top = 8
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=pokemon351'
      'User_Name=root'
      'DriverID=MySQL')
    Connected = True
    Left = 56
    Top = 64
  end
  object FDQueryTreinador: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'SELECT * FROM Treinador')
    Left = 56
    Top = 120
  end
  object DataSource1: TDataSource
    DataSet = FDQueryTreinador
    Left = 56
    Top = 184
  end
  object FDQueryPokemon: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'SELECT * FROM Pokemon')
    Left = 56
    Top = 240
  end
  object DataSource2: TDataSource
    DataSet = FDQueryPokemon
    Left = 56
    Top = 304
  end
end
